from django.shortcuts import render, redirect
from . models import *


def index(request):
    if request.method == "POST":
        args = request.POST
        if "message" in args and "accept-message" in args and args["message"] != "":
            message = Messages()
            message.message = args["message"]
            message.save()
        if "erase_all" in args:
            messages = Messages.objects.all()
            messages.delete()
        return redirect('/')
    messages = Messages.objects.all()
    context = {}
    context["notes"] = messages
    return render(request, 'index.html', context)
